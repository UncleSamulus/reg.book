# Gene Expression in Prokaryotes

A student-written course summary on gene expression regulation.*

See this current version compiled PDF document in [./doc](./doc); source code is in [./tex](./tex) folder.
