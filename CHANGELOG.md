# Changelog

## v0.0.1 *(2023-01-06)*

- [x] Transcription in Prokaryotes

- [ ] Translation in Prokaryotes 

- [ ] Gene Regulation in Prokaryotes

- [x] Gene Editing (CRISPR/Cas 9 system)

- [x] Glossary with some terminology

- [ ] Figures

<em><strong>Uncomplete</strong> version written before 2022 January Exam. Need further completion.</em>
